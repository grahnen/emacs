;;; useful-functions.el --- Useful functions -*- lexical-binding: t; -*-


(defun make-directory-if-not-exists (dir)
  "Create a directory if it does not exist"
  (if (not (file-directory-p dir))
      (make-directory dir)))


(defun eval-and-replace ()
  "Evaluate last sexp, and replace it with its results"
  (interactive)
  (backward-kill-sexp)
  (condition-case nil
      (prin1 (eval (read (current-kill 0)))
             (current-buffer))
    (error (message "Invalid Expression")
           (insert (current-kill 0)))))

(defmacro def-find-lambda (file)
  `(lambda () (interactive) (find-file ,file)))

(defmacro wk-file-lambda (file &optional wk)
  (if (eq wk t)
      `(lambda () (interactive) (find-file ,file))
    ``((lambda () (interactive) (find-file ,,file)) :which-key ,(file-name-nondirectory ,file))))


(defun eshell-here ()
  "Open a eshell window at current location"
  (interactive)
  (let* ((height (/ (window-total-height) 3))
	 (w (split-window-vertically (- height))))
    (select-window w)
    (eshell "new")
    (insert (concat "ls"))
    (eshell-send-input)))

(defun er-sudo-edit (&optional arg)
  "Edit currently visited file as root.
With a prefix ARG prompt for a file to visit.
Will also prompt for a file to visit if current
buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

(defun make-capture-frame ()
  "Create a new frame and run org-capture."
  (interactive)
  (make-frame '((name . "capture")))
  (select-frame-by-name "capture")
  (delete-other-windows)
  (noflet ((switch-to-buffer-other-window (buf) (switch-to-buffer buf)))
    (org-capture)))



(defun okular-make-url ()
  (concat
   "file://"
   (expand-file-name (funcall file (TeX-output-extension) t)
		     (file-name-directory (TeX-master-file)))
   "#src:"
   (TeX-current-line)
   (TeX-current-file-name-master-relative)))



(provide 'useful-functions)
