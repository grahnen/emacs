* About
This is a simple configuration using /use-package/, /general.el/ and /ivy/ to get a comfortable system.

Using the å,ä keys for custom shortcuts for maximum Swedishness.

* Installation
It's not very hard
#+BEGIN_SRC shell
git clone https://gitlab.com/grahnen/emacs.git ~/.emacs.d
#+END_SRC

Make sure to have AucTeX and Evince for LaTeX.

* [[file:config.org][Config]]
